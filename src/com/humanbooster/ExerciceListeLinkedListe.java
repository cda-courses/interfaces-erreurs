package com.humanbooster;

import java.util.LinkedList;
import java.util.Scanner;

public class ExerciceListeLinkedListe {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        LinkedList<String> listePatient = new LinkedList<String>();

        System.out.println("Combien de personnes attendent ?");

        int nbPatient = Integer.parseInt(scanner.nextLine());

        for (int i = 0; i<nbPatient; i++){
            int nbPatientAttente = i+1;
            System.out.println("Veuillez saisir le prénom du patient "+ nbPatientAttente);

            listePatient.add(scanner.nextLine());
        }

        while (listePatient.size()>0){
            System.out.println(listePatient.pollFirst());
        }
    }
}
