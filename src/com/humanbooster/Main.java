package com.humanbooster;

import com.humanbooster.Exceptions.PlafondException;
import com.humanbooster.Exceptions.SoldeInsuffisantException;
import com.humanbooster.cruds.CrudConcessionImplementation;
import com.humanbooster.models.CompteBancaire;
import com.humanbooster.models.Concession;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)  {
/*        Concession concession = new Concession();
        CrudConcessionImplementation crudConcessionImplementation = new CrudConcessionImplementation();
        // privcrudConcessionImplementation.create(concession);


        try {
            FileReader reader = new FileReader("fichierinconnu.pdf");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/

        CompteBancaire compteBancaire = new CompteBancaire("03436284", 1000000);

        // Ceci implique de traite notre exception
        // Ici on a 5000 euros sur le compte donc tout se passe bien
        try {
            compteBancaire.retirer(1000);
            System.out.println("YOUHOU voici 1000 euros !!!");
        } catch (SoldeInsuffisantException e) {
            System.out.println("Solde Insuffisant");
            e.printStackTrace();
        } catch (PlafondException plafondException){
            System.out.println("Vous avez atteint le plafond");
            plafondException.printStackTrace();
        }

        // Dans le deuxième cas, on a pas assez
        // Donc je passe dans mon catch
        // La JVM Va passer dans la classe CompteBancaire
        // La JVM Va passer par la classe SoldeInsuffisantException en créant un objet Exception
        // puisque la classe SoldeInsuffisantException hérite de Exception.
        try {
            compteBancaire.retirer(1500);
            System.out.println("YOUHOU voici 1500 euros !!!");
        } catch (SoldeInsuffisantException e) {
            System.out.println("Hello World");
            e.printStackTrace();
        } catch (PlafondException plafondException){
            plafondException.printStackTrace();
        }


    }
}
