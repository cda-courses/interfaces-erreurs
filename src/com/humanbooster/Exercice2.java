package com.humanbooster;

import com.humanbooster.services.Carre;
import com.humanbooster.services.Cercle;

public class Exercice2 {
    public static void main(String[] args)  {
        Carre carre = new Carre(10);
        System.out.println(carre.calculerSurface());

        Cercle cercle = new Cercle(10);
        System.out.println(cercle.calculerSurface());

    }
}
