package com.humanbooster.interfaces;

import com.humanbooster.models.Concession;

public interface CrudConcession<E> {
    Concession create(Concession concession);
    Concession get(int concessionId);
    Concession[] getAll();
    Concession update(Concession concession);
    void makeAction();
}
