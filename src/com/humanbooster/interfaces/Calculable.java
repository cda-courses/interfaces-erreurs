package com.humanbooster.interfaces;

public interface Calculable {
    float calculer(float a, float b) throws Exception;
}
