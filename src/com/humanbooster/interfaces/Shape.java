package com.humanbooster.interfaces;

public interface Shape {
    double calculerSurface();
}
