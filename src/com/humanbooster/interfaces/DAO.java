package com.humanbooster.interfaces;

import java.util.ArrayList;

public interface DAO<E> {
    public E ajout(E e);
    public boolean remove(E e);
    public ArrayList<E> getAll();
    public E getOne(double id);
    public E edit(double id, E object);
}
