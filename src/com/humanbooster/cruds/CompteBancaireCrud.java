package com.humanbooster.cruds;

import com.humanbooster.interfaces.DAO;
import com.humanbooster.models.CompteBancaire;

import java.util.ArrayList;

public class CompteBancaireCrud implements DAO<CompteBancaire> {
    @Override
    public CompteBancaire ajout(CompteBancaire compteBancaire) {
        return null;
    }

    @Override
    public boolean remove(CompteBancaire compteBancaire) {
        return false;
    }

    @Override
    public ArrayList<CompteBancaire> getAll() {
        return null;
    }

    @Override
    public CompteBancaire getOne(double id) {
        return null;
    }

    @Override
    public CompteBancaire edit(double id, CompteBancaire object) {
        return null;
    }

}
