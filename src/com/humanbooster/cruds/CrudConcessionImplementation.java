package com.humanbooster.cruds;

import com.humanbooster.interfaces.CrudConcession;
import com.humanbooster.models.Concession;

public class CrudConcessionImplementation implements CrudConcession {

    @Override
    public Concession create(Concession concession) {
        return null;
    }

    @Override
    public Concession get(int concessionId) {
        return null;
    }

    @Override
    public Concession[] getAll() {
        return new Concession[0];
    }

    @Override
    public Concession update(Concession concession) {
        return null;
    }

    @Override
    public void makeAction() {

    }
}
