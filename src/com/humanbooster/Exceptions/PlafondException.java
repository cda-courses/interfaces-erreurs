package com.humanbooster.Exceptions;

public class PlafondException extends Exception{
    public PlafondException(String message){
        super(message);
        System.out.println("Le plafond est dépassé !");
    }
}
