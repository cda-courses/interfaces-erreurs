package com.humanbooster.Exceptions;

// Nous avons ici une classe qui hérite de Exception
public class SoldeInsuffisantException extends Exception {

    public String numCompte;

    // Dans le constructeur, on appel le constructeur du parent
    // puis on ajoute une action spécifique à cette exception
    public SoldeInsuffisantException(String message, String numCompte){
        super(message);
        this.numCompte = numCompte;
    }
}
