package com.humanbooster.models;

public class Concession {
    private int nbParassol;
    private int nbEmplacement;
    private String nomGerant;

    public int getNbParassol() {
        return nbParassol;
    }

    public void setNbParassol(int nbParassol) {
        this.nbParassol = nbParassol;
    }

    public int getNbEmplacement() {
        return nbEmplacement;
    }

    public void setNbEmplacement(int nbEmplacement) {
        this.nbEmplacement = nbEmplacement;
    }

    public String getNomGerant() {
        return nomGerant;
    }

    public void setNomGerant(String nomGerant) {
        this.nomGerant = nomGerant;
    }
}
