package com.humanbooster.models;

public class Person {
    private int age;

    public void setAge(int age) throws IllegalArgumentException {
        if(age < 0 || age > 120){
            throw new IllegalArgumentException("Cet age n'est pas valide");
        }
        this.age = age;
    }
}
