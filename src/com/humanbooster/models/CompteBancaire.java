package com.humanbooster.models;

import com.humanbooster.Exceptions.PlafondException;
import com.humanbooster.Exceptions.SoldeInsuffisantException;

public class CompteBancaire {
    private String numero;
    private double solde;
    private double plafond;

    public CompteBancaire(String numero, double solde) {
        this.numero = numero;
        this.solde = solde;
        this.plafond = 2000;
    }

    // Dans la méthode retirer, on décide de lancer une exception SoldeInsuffisant
    // Si le montant du retrait est supérieur au solde du compte
    public void retirer(double montant) throws SoldeInsuffisantException, PlafondException {
        if(montant > solde){
            throw new SoldeInsuffisantException("Le solde est insuffisant !", this.numero);
        }

        if(montant>plafond){
            throw new PlafondException("Le plafond est dépassé");
        }
        this.solde -= montant;
        this.plafond -= montant;
    }
}
