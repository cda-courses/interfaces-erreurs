package com.humanbooster.services;

import java.util.ArrayList;

public class ListGenerique<E> {
    ArrayList<E> table;

    public ListGenerique(){
        table = new ArrayList<E>();
    }

    public void addElement(E element){
        table.add(element);
    }
}
