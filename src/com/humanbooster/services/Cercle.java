package com.humanbooster.services;

import com.humanbooster.interfaces.Shape;

public class Cercle implements Shape {
    public double rayon;

    public Cercle(double rayon){
        this.rayon = rayon;
    }

    public double calculerSurface(){
        return Math.PI * (this.rayon*this.rayon);
    }
}
