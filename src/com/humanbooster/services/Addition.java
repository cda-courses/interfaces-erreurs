package com.humanbooster.services;

import com.humanbooster.interfaces.Calculable;

public class Addition implements Calculable {

    @Override
    public float calculer(float a, float b) {
        return a + b ;
    }
}
