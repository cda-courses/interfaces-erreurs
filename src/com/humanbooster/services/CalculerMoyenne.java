package com.humanbooster.services;

public class CalculerMoyenne {

    public float calculerMoyenne(int[] nombres){
        if(nombres == null || nombres.length == 0 ){
            throw new IllegalArgumentException("Le tableau ne doit pas être vide");
        }

        Integer somme = 0;
        for (int nbr:nombres ){
            somme = somme + nbr;
        }
        float sommeFloat = somme.floatValue();
        return sommeFloat / nombres.length;
    }
}
