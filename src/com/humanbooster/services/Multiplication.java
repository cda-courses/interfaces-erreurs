package com.humanbooster.services;

import com.humanbooster.interfaces.Calculable;

public class Multiplication implements Calculable {
    @Override
    public float calculer(float a, float b) {
        return a*b;
    }
}
