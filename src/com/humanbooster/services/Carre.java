package com.humanbooster.services;

import com.humanbooster.interfaces.Shape;

public class Carre implements Shape {
    double cote;

    public Carre(double cote){
        this.cote = cote;
    }

    @Override
    public double calculerSurface() {
        return this.cote * this.cote ;
    }
}
