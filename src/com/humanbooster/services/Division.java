package com.humanbooster.services;

import com.humanbooster.interfaces.Calculable;

public class Division implements Calculable {
    @Override
    public float calculer(float a, float b) throws Exception {
        if(b == 0){
            throw new Exception("Impossible ! Division par 0");
        }
        return a/b;
    }
}
