package com.humanbooster;


import java.util.InputMismatchException;
import java.util.Scanner;

public class ErreurExo1 {
    public static void main(String[] args)  {
        System.out.println("Veuillez saisir un entier");
        Scanner scanner = new Scanner(System.in);
        int nbSaisie;
        try {
            nbSaisie = scanner.nextInt();
            System.out.println(nbSaisie);
        } catch (InputMismatchException exception){
            String[] argsMain = {};
            main(argsMain);
        }
    }
}
