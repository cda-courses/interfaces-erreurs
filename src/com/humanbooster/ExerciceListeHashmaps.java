package com.humanbooster;

import java.util.HashMap;
import java.util.Scanner;

public class ExerciceListeHashmaps {
    public static void main(String[] args){
        HashMap<String, Integer> animals = new HashMap<String, Integer>();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Veuillez saisir des animaux séparés par des espaces");

        String saisie = scanner.nextLine();

        String[] listeSaisie = saisie.split(" ");

        for(int i = 0; i < listeSaisie.length; i++) {
           if( animals.containsKey(listeSaisie[i]) ){
               int count = animals.get(listeSaisie[i]);
               animals.put(listeSaisie[i], count + 1);
           } else {
               animals.put(listeSaisie[i], 1);
           }
        }

        for(String s: animals.keySet()){
            System.out.println(s +" "+ animals.get(s));
        }
    }
}
