package com.humanbooster;

import com.humanbooster.services.Addition;
import com.humanbooster.services.Division;
import com.humanbooster.services.Multiplication;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercice1 {
    public static void main(String[] args) throws Exception  {
        System.out.println("Veuillez saisir le signe souhaité (* ou + ou /)");

        Scanner scanner = new Scanner(System.in);
        String saisie = scanner.nextLine();

        if( !saisie.equals("*") && !saisie.equals("+") && !saisie.equals("/")){
            throw new Exception("La calculette ne prend pas compte cette opération");
        }

        System.out.println("Veuillez saisie le premier nombre");
        float nombre1;

        try {
             nombre1 = scanner.nextFloat();
        } catch (InputMismatchException e){
            throw new Exception("Le nombre 1 n'est pas correct !");
        }

        System.out.println("Veuillez saisie le deuxième nombre");
        float nombre2;
        try{
            nombre2 = scanner.nextFloat();
        } catch (InputMismatchException e){
            throw new Exception("Le nombre 2 n'est pas correcte");
        }

        float resultat = 0;

        if(saisie.equals("+")){
            Addition addition = new Addition();
            resultat = addition.calculer(nombre1, nombre2);
            System.out.print("Résultat de l'addition ");
        }

        if(saisie.equals("*")){
            Multiplication multiplication = new Multiplication();
            resultat = multiplication.calculer(nombre1, nombre2);
            System.out.print("Résultat de la multiplication ");
        }

        if(saisie.equals("/")){
            Division division = new Division();

            try {
                resultat = division.calculer(nombre1, nombre2);
            } catch (Exception e){
                System.out.println("Impossible de faire une division par 0");
                return;
            }
        }

        System.out.print(resultat);
    }
}
